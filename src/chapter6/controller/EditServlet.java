package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

//editというURLと対応している（リクエストがあった時に、実行される）
@WebServlet(urlPatterns = { "/edit" })
//HTTPServletクラスを継承したEditServletクラスの作成。
public class EditServlet extends HttpServlet {

	//実装部分を下記に記載していく。
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)//doGetはDBに変更を加えない情報を取得したい
            throws IOException, ServletException {

    	HttpSession session = request.getSession();

    	//int型のmessageIdの値をreqest.getParameter("messageId")でJSPから受け取るように設定。
    	//パラメータ（Parameter）でmessageIdを取得（get）
        String messageId = request.getParameter("messageId");

        //MessageServiceクラスからmessageIdを検索し取ってくる
        Message message = new MessageService().select(messageId);

        List<String> errorMessages = new ArrayList<String>();

        if(!(messageId.matches("[0-9]+")) || message == null) {
        	errorMessages.add("不正なパラメータが検出されました");
        	session.setAttribute("errorMessages", errorMessages);
        	response.sendRedirect("./");
        	return;
        }


        //リクエストにmessageというキーで値をセットし、/edit.jspから参照できるようにする。
        request.setAttribute("message", message);
        request.getRequestDispatcher("/edit.jsp").forward(request, response);//Editのページへ移動を指定する
    }

    @Override//doPostはDBに変更、修正、更新を加えるときに使用するメソッド
    protected void doPost(HttpServletRequest request, HttpServletResponse response)//Edit.jspから更新したメッセージを受け取る
            throws IOException, ServletException {



        //String型でerrorMessageというListを生成する
        List<String> errorMessages = new ArrayList<String>();

        //入力したつぶやきをパラメーターで取ってくる。
        String text = request.getParameter("text");

        //ﾊﾟﾗﾒｰﾀでﾘｸｴｽﾄされたﾒｯｾｰｼﾞIDを取ってくる。
        String messageId = request.getParameter("messageId");

        //バリデーション
        if (!isValid(text, errorMessages)) {
        	Message message = new MessageService().select(messageId);

        	request.setAttribute("errorMessages", errorMessages);
        	request.setAttribute("message", message);
            request.getRequestDispatcher("edit.jsp").forward(request, response);

            return;
        }

        //messageにedit.jspで入力したつぶやきをセットする
        Message message = new Message();
        message.setText(text);
        message.setId(messageId);

        //メッセージの更新をする（update）
        new MessageService().update(message);

        response.sendRedirect("./");//トップページに戻る
    }

    private boolean isValid(String text, List<String> errorMessages) {

        if (StringUtils.isBlank(text)) {
            errorMessages.add("メッセージを入力してください");
        } else if (140 < text.length()) {
            errorMessages.add("140文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}