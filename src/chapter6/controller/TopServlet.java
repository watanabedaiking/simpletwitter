package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.User;
import chapter6.beans.UserComment;
import chapter6.beans.UserMessage;
import chapter6.service.CommentService;
import chapter6.service.MessageService;

//index.jspというURLと対応している（リクエストがあった時に、実行される）
//ディレクトリにアクセスしたときに、Tomcatではデフォルトでindex.jspを開くように設定されている。
@WebServlet(urlPatterns = { "/index.jsp" })


//HTTPServletクラスを継承したTopServletクラスの作成。
public class TopServlet extends HttpServlet {

	//実装部分を下記に記載していく。
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        boolean isShowMessageForm = false;
        boolean isShowCommentForm = false;
        User user = (User) request.getSession().getAttribute("loginUser");
        if (user != null) {
            isShowMessageForm = true;
            isShowCommentForm = true;
        }
        //String型のuser_idの値をreqest.getParameter("user_id")でJSPから受け取るように設定。
        String userId = request.getParameter("user_id");

        String start = request.getParameter("start");
        String end = request.getParameter("end");

        //MessageServiceのgetMessageに引数として、String型のuserIdを追加。
        List<UserMessage> messages = new MessageService().select(userId,start, end);

        //リクエストにmessageというキーで値をセットし、/top.jspから参照できるようにする。
        request.setAttribute("messages", messages);
        request.setAttribute("isShowMessageForm", isShowMessageForm);
        request.setAttribute("start", start);
        request.setAttribute("end", end);


        //CommentServiceからコメントを取ってくるように設定
        //トップ画面に表示するように設定
        List<UserComment> comments = new CommentService().select();


        //System.out.println(comments.get(0).getText());
        request.setAttribute("comments", comments);
        request.setAttribute("isShowCommentForm", isShowCommentForm);

        request.getRequestDispatcher("/top.jsp").forward(request, response);
    }
}