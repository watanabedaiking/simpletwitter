package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.Message;
import chapter6.service.MessageService;



@WebServlet(urlPatterns = { "/deleteMessage" })
public class DeleteMessageServlet  extends HttpServlet {

	//EditServlet→TopServletへの流れと同じ
	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

		//messageIdをパラメーターとして受け取る
		String messageId = request.getParameter("messageId");
		Message message =  new MessageService().select(messageId);

		//messageの消去をする
		new MessageService().delete(message);

		//top画面に戻る
		response.sendRedirect("./");
	}
}
