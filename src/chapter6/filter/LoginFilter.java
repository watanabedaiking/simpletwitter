package chapter6.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;


//全部のページフィルターをかける
@WebFilter({"/setting","/edit"})
public class LoginFilter implements Filter {


	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

			//doFilterの引数をHttpServletRequestとHttpServletResponseに型変換
			HttpServletRequest HttpRequest = (HttpServletRequest)request;
			HttpServletResponse HttpResponse = (HttpServletResponse)response;

			//セッション領域のログインユーザーの情報
			HttpSession session = HttpRequest.getSession();

			User user = (User) HttpRequest.getSession().getAttribute("loginUser");

			//リクエスト中のServlet名だけ知りたい
			String ServletPath = HttpRequest.getServletPath();

			//ログインしているか判定し、遷移する画面を分岐する
			if(ServletPath.equals("/login") || user != null) {
				chain.doFilter(request, response);

			//そうじゃなかったら、ログイン画面に遷移
			 }else {
					List<String> errorMessages = new ArrayList<String>();
					errorMessages.add("ログインしてください");
					session.setAttribute("errorMessages", errorMessages);
					HttpResponse.sendRedirect("./login");
				}
	}

	@Override
	public void init(FilterConfig config) {

	}

	@Override
	public void destroy() {
	}

}
