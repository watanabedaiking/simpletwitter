package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Message;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class MessageDao {
//ホーム画面のつぶやき機能
  public void insert(Connection connection, Message message) {//insertは新しくデータを登録する

    PreparedStatement ps = null;
    try {
      StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("    user_id, ");
            sql.append("    text, ");
            sql.append("    created_date, ");
            sql.append("    updated_date ");
            sql.append(") VALUES ( ");
            sql.append("    ?, ");                                  // user_id
            sql.append("    ?, ");                                  // text
            sql.append("    CURRENT_TIMESTAMP, ");  // created_date
            sql.append("    CURRENT_TIMESTAMP ");       // updated_date
            sql.append(")");

      ps = connection.prepareStatement(sql.toString());

      ps.setInt(1, message.getUserId());
      ps.setString(2, message.getText());

      ps.executeUpdate();
    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
  }

  private List<Message> toMessages(ResultSet rs) throws SQLException{//編集のデータを受け渡す処理

      List<Message> messages = new ArrayList<Message>();
      try {
          while (rs.next()) {
              Message message = new Message();
              message.setId(rs.getString("id"));
              message.setText(rs.getString("text"));
              message.setCreatedDate(rs.getTimestamp("created_date"));
              message.setUpdatedDate(rs.getTimestamp("updated_date"));

              messages.add(message);//List<Message>に値を追加する処理がadd
          }
          return messages;
      } finally {
          close(rs);
      }
}

  public Message select(Connection connection, String messageId) {//編集画面のメッセージIDの取得

      PreparedStatement ps = null;
      try {
        String sql = "SELECT * FROM messages WHERE id = ?";//DBへの指定した内容の取得。ここではmessageTABLEからidの取得をする。
        ////PreraredStatementはJDBCのSQL文を前もって準備しておくことで性能を向上させるために書く。
        ps = connection.prepareStatement(sql);

        ps.setString(1, messageId);

        ResultSet rs = ps.executeQuery();//executeQueryは必ずセレクト文を使ったら値取得に必要な構文

        List<Message> message = toMessages(rs);
        if (message.isEmpty()) {
          return null;
        } else {
          return message.get(0);
        }
      } catch (SQLException e) {
        throw new SQLRuntimeException(e);
      } finally {
        close(ps);
      }
    }


public void update(Connection connection, Message message) {//updateはデータを新しく更新する。

		//PreraredStatementはJDBCのSQL文を前もって準備しておくことで性能を向上させるために書く。
	    PreparedStatement ps = null;
	    try {
	      StringBuilder sql = new StringBuilder();

	      //StringBuilderオブジェクトにappend以降の内容が追加されていく
	            sql.append(" UPDATE messages SET ");
	            sql.append("    text = ?, ");
	            sql.append("    updated_date = CURRENT_TIMESTAMP ");
	            sql.append("WHERE id = ? ");


	      ps = connection.prepareStatement(sql.toString());

	      ps.setString(1, message.getText());
	      ps.setString(2, message.getId());

          int count = ps.executeUpdate();
          if (count == 0) {
              throw new NoRowsUpdatedRuntimeException();
          }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	  }

public void delete(Connection connection, Message message) {//messageの消去処理。

	//PreraredStatementはJDBCのSQL文を前もって準備しておくことで性能を向上させるために書く。
    PreparedStatement ps = null;
    try {
      StringBuilder sql = new StringBuilder();

      //注意：deleteを使うとレコードが消えるから、idをひとつ選択して削除してあげる。
      sql.append("DELETE FROM messages WHERE id = ?");

      ps = connection.prepareStatement(sql.toString());

      ps.setString(1, message.getId());

      int count = ps.executeUpdate();
      if (count == 0) {
          throw new NoRowsUpdatedRuntimeException();
      }
    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
  }
}