package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {
//insertメソッドは文字列の挿入したいときに使用。
    public void insert(Message message) {//メッセージの投稿をする

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    //selectメソッドの引数にString型のuserIdを追加
    public List<UserMessage> select(String userId, String start, String end) {//ホーム画面でのメッセージの表示
        final int LIMIT_NUM = 1000;
        //idをnullで初期化
        //servletからuserIdの値が渡ってきていたら、整数型に型変換し、idに代入
        Integer id = null;

        SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date today = new Date();

        Connection connection = null;
        try {
            connection = getConnection();

            if(!StringUtils.isEmpty(userId)) {
            	id = Integer.parseInt(userId);
            }
            //messageDaoクラスのselectメソッドに引数としてinteger型のidを追加
            //idがnullだったら全件取得する
            //idがnull以外だったら、その値に対応するユーザーIDの投稿を取得する

            if(!StringUtils.isEmpty(start)) {
            	start = start + " 00:00:00";
            }else {
            	start = "2020-01-01 00:00:00";
            }

            if(!StringUtils.isEmpty(end)) {
            	end = end + " 23:59:59";
            }else {
            	end = simpledateformat.format(today);
            }


            List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, start, end);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public Message select(String messageId) {//編集画面のmessageIdの値を受け渡す処理

        Connection connection = null;
        try {
            connection = getConnection();

            Message message = new MessageDao().select(connection, messageId);
            commit(connection);

            return message;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

  //updateメソッドは更新したいときに使用。
    public void update(Message message) {//メッセージの編集

        Connection connection = null;
        try {
        	//データベース接続
            connection = getConnection();
            new MessageDao().update(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //deleteメソッドは消去したいときに使用
    public void delete(Message message) {//メッセージの消去
        Connection connection = null;
        try {
        	//データベース接続
            connection = getConnection();
            new MessageDao().delete(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}