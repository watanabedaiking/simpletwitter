<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>つぶやき編集画面</title>
        <link href="C:/Users/watanabe.daiki/workspace/SimpleTwitter/WebContent/css/style.css" rel="stylesheet" type="text/css">
    </head>

    <body>
    <%--main部分の作成 --%>
        <div class="main-contents">

		<c:if test="${ not empty errorMessages }">
    <div class="errorMessages">
        <ul>
            <c:forEach items="${errorMessages}" var="errorMessage">
                <li><c:out value="${errorMessage}" />
            </c:forEach>
        </ul>
    </div>
    <c:remove var="errorMessages" scope="session" />
</c:if>

<%--つぶやき編集画面を下記に記載 --%>
<%--edit-areaという編集画面のクラスを作成 --%>
<div class="edit-area">
		<%--edit.jspに編集（書き込み）をしたいから、methodはpostを使う --%>
        <form action="edit" method="post">
            <label for = "text">編集</label>
            <textarea name="text" cols="90" rows="5" class="tweet-box">${message.text}</textarea>
            <br />
        	<input name ="messageId" value="${message.id}" id = "id"  type= "hidden"/>

			<%--typeは機能追加、submitは送信、valueは機能で表示する名前 --%>
			<%--まとめると更新という表示をした送信機能の追加 --%>
            <input type="submit" value="更新"> <br />
            <%--./はトップページに戻ることを指定している（TopServletでデフォルトで設定してる --%>
            <%--どこのページに戻るか指定する（今回は./） --%>
            <a href="./">戻る</a>
        </form>
</div>

            <div class="copyright"> Copyright(c)DaikiWatanabe</div>
        </div>
    </body>
</html>
