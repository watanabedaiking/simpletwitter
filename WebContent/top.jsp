<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>簡易Twitter</title>
        <link href="C:/Users/watanabe.daiki/workspace/SimpleTwitter/WebContent/css/style.css" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

        <script>
        	$(function(){
        		$('#delete').on('click',function(){
        			alert('消去しますか？');
        		});
        	});
        </script>
    </head>


    <body>
        <div class="main-contents">
 		<div class="header">
    		<c:if test="${ empty loginUser }">
        		<a href="login">ログイン</a>
        		<a href="signup">登録する</a>
    		</c:if>
    		<c:if test="${ not empty loginUser }">
        		<a href="./">ホーム</a>
        		<a href="setting">設定</a>
        		<a href="logout">ログアウト</a>
    		</c:if>
		</div>

		<%--つぶやきの絞り込みボタンの作成 --%>
		<div class = "narrow-down">
			<form action = "./" method = "get">
				<input type = "date" name = "start" value = "${start}" min = "2020-01-01">
				～
				<input type = "date" name = "end" value = "${end}">
				<input type = "submit" value = "絞込">
			</form>
		</div>

		<c:if test="${ not empty loginUser }">
    		<div class="profile">
        		<div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
        		<div class="account">@<c:out value="${loginUser.account}" /></div>
        		<div class="description"><c:out value="${loginUser.description}" /></div>
    		</div>
		</c:if>


		<c:if test="${ not empty errorMessages }">
    		<div class="errorMessages">
        		<ul>
            		<c:forEach items="${errorMessages}" var="errorMessage">
                		<li><c:out value="${errorMessage}" />
            		</c:forEach>
        		</ul>
    		</div>
    		<c:remove var="errorMessages" scope="session" />
		</c:if>

		<div class="form-area">
    		<c:if test="${ isShowMessageForm }">
        		<form action="message" method="post">
            		いま、どうしてる？<br />
            		<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
            		<br />
            		<input type="submit" value="つぶやく">（140文字まで）
        		</form>
    		</c:if>
		</div>


		<div class="messages">
    		<c:forEach items="${messages}" var="message">
        		<div class="message">
            	<div class="account-name">
                	<span class="account">
                		<a href="./?user_id=<c:out value="${message.userId}"/> ">
                		<c:out value="${message.account}" /></a></span>
                	<span class="name"><c:out value="${message.name}" /></span>
            	</div>

        <c:forEach var="r" items="${fn:split(message.text, '
')}">
			<div class = "text">
				<c:out value = "${r}"/>
			</div>
		</c:forEach>

            <div class="date"><fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>


			<div style="display:inline-flex">
            <%--自分のアカウントだけ編集ボタンが欲しい（他人のアカウントは編集できないようにする） --%>
            <%--ログインしているユーザー名と下記に表示されているメッセージのユーザー名が一致した場合 --%>
    		<div class = "edit">
    			<c:if test="${ loginUser.name == message.name }">
    				<%--edit.jspの画面を表示させたい --%>
    				<%--form actionは遷移するページのedit.jsp、画面を表示させたいだけの処理だからmethodはgetを使う --%>
        			<form action="edit" method="get">
            			<%--message_idを送りたいタグを書く inputタグの送信の役割を使って書いてみる --%>
						<input name = "messageId" value = "${message.id}" type= "hidden"><br>
            			<input type="submit" value="編集">
            		</form>
    			</c:if>
    		</div>

			<%--自身つぶやきだけ、削除ボタンを作って、削除できるようにする --%>
            <div class = "delete">
            	<c:if test="${ loginUser.name == message.name }">
    				<%--削除を行いたいから、method = postを使用する --%>
        			<form action="deleteMessage" method="post">
            			<%--message_idを送りたいタグを書く inputタグの送信の役割を使って書いてみる --%>
						<input name = "messageId" value = "${message.id}" type= "hidden"><br>
            			<input type="submit" value="削除">
            		</form>
            	</c:if>
    		</div>
			</div>


			<%--返信の表示をする --%>
			<div class = "comments">
    			<c:forEach items="${comments}" var="comment">
    				<c:if test="${ comment.messageId == message.id }">

            			<div class="account-name">
                			<span class="account">
                				<c:out value="${comment.account}" />
                			</span>

                			<span class="name">
                				<c:out value="${comment.name}" />
                			</span>

							<c:forEach var="r" items="${fn:split(comment.text, '
')}">
								<div class = "text">
									<c:out value = "${r}"/>
								</div>
							</c:forEach>

            				<div class="date">
            					<fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
            				</div>
						</div>
					</c:if>
				</c:forEach>
			</div>

					<c:if test="${ isShowCommentForm }">
						<div class = "comment">
							<form action="comment" method="post">
								<input name = "messageId" value = "${message.id}" type= "hidden"><br>
				    			返信<br>
            						<textarea name="comment" cols="90" rows="5" class="comment-box"></textarea>
            						<br />
								<input type = "submit" value = "返信">
							</form>
						</div>
					</c:if>

				</div>
			</c:forEach>
        </div>
    </div>
            <div class="copyright"> Copyright(c)DaikiWatanabe</div>

    </body>
</html>
